const {Command} = require("discord.js-commando");

module.exports = class JoinCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'join',
            group: 'music',
            memberName: 'join',
            description: 'Rejoint un salon vocal',
        });
    }

    /**
     *
     * @param message
     */
    async run(message) {
        const voiceChannel = message.member.voice.channel;
        if (!voiceChannel) {
            return message.say('Rejoins un salon vocal pour utiliser cette commande.');
        }

        await voiceChannel.join()

        return message.say(":thumbsup: Je suis maintenant dans le channel " + "'" + voiceChannel.name + "'");
    }
}
