const {Command} = require("discord.js-commando");
const ytdl = require("ytdl-core-discord");
const sqlite3 = require("sqlite3").verbose();

module.exports = class PlayCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'play',
            aliases: ['p'],
            group: 'music',
            memberName: 'play',
            description: 'Lit une musique depuis youtube',
            args: [
                {
                    key: 'query',
                    prompt: 'Quelle musique faut-il jouer?',
                    type: 'string'
                }
            ]
        });
    }

    /**
     *
     * @param message
     * @param querry
     */
    async run(message, {query}) {

        if (!message.member.voice.channel) {
            return message.say('Rejoins un salon vocal pour utiliser cette commande.');
        }
        const server = message.client.server;
        await message.member.voice.channel.join().then(async (connection) => {
            let title = (await ytdl.getBasicInfo(query)).videoDetails.title;
            const db = new sqlite3.Database(server.databaseFile);
            db.run(
                `INSERT INTO requests (userId, userName, videoName, videoLink) VALUES ('${message.member.id}', '${message.member.displayName}', '${title}', '${query}');`
            );
            db.close();
            if (server.currentVideo.url !== "") {
                server.queue.push({title: title, url: query})
                return message.say("Ajouté à la file d'attente");
            }
            server.currentVideo = {title: title, url: query};
            this.runVideo(message, connection, query);
        });
    }

    /**
     *
     * @param message
     * @param connection
     * @param video
     * @returns {Promise<void>}
     */
    async runVideo(message, connection, video) {

        const server = message.client.server;
        const dispatcher = connection.play(await ytdl(video, {filter: 'audioonly'}), {type: 'opus'});

        server.queue.shift();
        server.dispatcher = dispatcher;
        server.playing = true;

        dispatcher.on('finish', () => {
            if (server.queue[0]) {
                server.currentVideo = server.queue[0];
                return this.runVideo(message, connection, server.currentVideo.url)
            } else {
                server.currentVideo = {title: "", url: ""};
                return message.say("Fin de lecture.");
            }
        });
        return message.say("En train de jouer :notes: ")
    }
}
