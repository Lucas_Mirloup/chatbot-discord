const {Command} = require("discord.js-commando");

module.exports = class ResumeCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'resume',
            group: 'music',
            memberName: 'resume',
            description: 'Reprend la lecture la lecture',
        });
    }

    /**
     *
     * @param message
     */
    async run(message) {
        const dispatcher = message.client.server.dispatcher;
        if (!message.member.voice.channel) {
            return message.say('Rejoins un salon vocal pour utiliser cette commande.');
        }

        if (!message.guild.me.voice.channel) {
            return message.say('Je ne suis pas connecté à un salon vocal.');
        }

        if (dispatcher && message.client.server.playing) {
            return message.say("Il n'y a pas de musique en pause.");
        }

        if (dispatcher && !message.client.server.playing) {
            dispatcher.resume();
            message.client.server.playing = true;
        }

        return message.say("Reprise de la musique");
    }
}
