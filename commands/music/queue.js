const {MessageEmbed} = require("discord.js");
const {Command} = require("discord.js-commando");

module.exports = class QueueCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'queue',
            group: 'music',
            memberName: 'queue',
            description: "Affiche la file d'attente. Pour afficher la file d'attente tape le commande queue suivie du numero de la page",
            args: [
                {
                    key: 'page',
                    prompt: "Quelle page veut-tu afficher ?",
                    default: 1,
                    type: 'integer'
                }
            ]
        });
    }

    /**
     *
     * @param message
     * @param page
     */
    async run(message, {page}) {
        const server = message.client.server;

        if (!message.guild.me.voice.channel) {
            return message.say('Je ne suis pas connecté à un salon vocal.');
        }

        if (server.currentVideo.url === "") {
            return message.say("Il n'y a aucune vidéo dans la file d'attente")
        }

        const numberOfMusic = 10;
        const startingMusic = (page - 1) * numberOfMusic;
        const queueLength = server.queue.length;

        var musicPerPage = startingMusic + numberOfMusic;
        var totalPages = 1;

        if (queueLength > numberOfMusic) {
            totalPages = Math.ceil(queueLength / numberOfMusic);
        }

        if (page <= 0 || page > totalPages) {
            return message.say(":x: cette page n'existe pas");
        }

        var embed = new MessageEmbed()
            .setTitle("File d'attente")
            .setColor('BLUE')
            .addField('En train de jouer: ', server.currentVideo.url);

        if (queueLength > 0) {
            var value = "";

            if ((queueLength - startingMusic) < numberOfMusic) {
                musicPerPage = (queueLength - startingMusic) + startingMusic;
            }

            for (let i = startingMusic; i < musicPerPage; i++) {
                const music = server.queue[i];
                value += "`" + (i + 1) + ".` " + music.url + "\n";
            }

            embed.addField("A venir : ", value);
        }

        embed.setFooter('Page ' + page + "/" + totalPages);

        return message.say(embed);
    }
}
