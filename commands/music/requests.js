const {MessageEmbed} = require("discord.js");
const {Command} = require("discord.js-commando");
const sqlite3 = require("sqlite3").verbose();

module.exports = class RequestsCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'requests',
            aliases: ['request'],
            group: 'music',
            memberName: 'requests',
            description: "Affiche l'historique des requetes. Pour afficher l'historique' tape le commande requests suivie du numero de la page",
            args: [
                {
                    key: 'page',
                    prompt: "Quelle page veut-tu afficher ?",
                    default: 1,
                    type: 'integer'
                }
            ]
        });
    }

    /**
     *
     * @param message
     * @param page
     */
    async run(message, {page}) {
        const server = message.client.server;

        const videoList = [];
        const db = new sqlite3.Database(server.databaseFile);

        let res = "";

        db.all(
            "SELECT userId, userName, videoName, videoLink FROM requests ORDER BY id ASC;", [], (err, rows) => {
                if (err) {
                    return message.say("Erreur lors de la récupération des données: " + err)
                }

                rows.forEach(row => {
                    videoList.push(row);
                    res += JSON.stringify(row);
                    console.log(res);
                });
                const numberOfMusic = 10;
                const startingMusic = (page - 1) * numberOfMusic;
                const queueLength = videoList.length;

                let musicPerPage = startingMusic + numberOfMusic;
                let totalPages = 1;

                if (queueLength > numberOfMusic) {
                    totalPages = Math.ceil(queueLength / numberOfMusic);
                }

                if (page <= 0 || page > totalPages) {
                    return message.say(":x: cette page n'existe pas");
                }

                const embed = new MessageEmbed()
                    .setTitle("Historique des requêtes")
                    .setColor('BLUE');

                if (queueLength > 0) {
                    let value = "";

                    if ((queueLength - startingMusic) < numberOfMusic) {
                        musicPerPage = (queueLength - startingMusic) + startingMusic;
                    }

                    for (let i = startingMusic; i < musicPerPage; i++) {
                        const {userName, videoName, videoLink} = videoList[i];
                        value += `\`${i + 1}.\` [${videoName}](${videoLink}) par ${userName}`;
                    }

                    embed.addField("Historique: ", value);
                }

                embed.setFooter('Page ' + page + "/" + totalPages);

                return message.say(embed);
            }
        );
        db.close();
    }
}
