const {Command} = require("discord.js-commando");

module.exports = class PauseCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'pause',
            group: 'music',
            memberName: 'pause',
            description: 'Met en pause la lecture',
        });
    }

    /**
     *
     * @param message
     */
    async run(message) {
        const dispatcher = message.client.server.dispatcher;
        if (!message.member.voice.channel) {
            return message.say('Rejoins un salon vocal pour utiliser cette commande.');
        }

        if (!message.guild.me.voice.channel) {
            return message.say('Je ne suis pas connecté à un salon vocal.');
        }

        if (dispatcher && !message.client.server.playing) {
            return message.say("Il n'y a pas de musique en cours de lecture.");
        }

        if (dispatcher && message.client.server.playing) {
            dispatcher.pause();
            message.client.server.playing = false;
        }

        return message.say(":pause_button: Pause");
    }
}
