const {CommandoClient} = require('discord.js-commando');
const path = require('path');
const sqlite3 = require("sqlite3").verbose();
const fs = require("fs");

const dbFile = './db.sqlite';
let dbExists = fs.existsSync(dbFile);


if (!dbExists) {
    const db = new sqlite3.Database(dbFile);
    db.run(
        'CREATE TABLE requests(' +
        'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,' +
        'userId INTEGER,' +
        'userName VARCHAR(50),' +
        'videoName VARCHAR(255),' +
        'videoLink BLOB)'
    )
    db.close();
}

const client = new CommandoClient({
    commandPrefix: '*',
    owner: '205637258013835265',
    invite: 'https://discord.gg/xkP2dnQ4w4'
});

client.registry
    .registerDefaultTypes()
    .registerDefaultGroups()
    .registerDefaultCommands()
    .registerGroup('music', 'Music')
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.server = {
    queue: [],
    currentVideo: {title: "", url: ""},
    dispatcher: null,
    playing: false,
    databaseFile: dbFile
};

client.once('ready', () => {
    console.log(`Connecté en tant que "${client.user.tag}" - (${client.user.id})`);
})

client.on('error', (error) => console.error(error));

client.login('ODAxNDc2OTQ3NzUzNTY2MjA5.YAhPkw.UDv2o5CEp3tyyO_TLXGWnDzf5Kk');
