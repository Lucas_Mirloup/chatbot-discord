# Chatbot Discord

## Utilisation du bot musique sur Discord:

### Prérequis :
- Installez NodeJS et npm.
- Dans un terminal, placez-vous dans le dossier du bot puis utilisez la commande `npm install`.
- Connectez-vous à un serveur Discord dans lequel le bot est présent pour pouvoir le tester.
- (Connectez-vous à un salon vocal, la plupart des commandes requiert que l'utilisateur soit connecté à un salon vocal).

### Utilisation :
Dans un terminal, placez-vous dans le dossier du bot et utilisez la commande `node index.js`.

Lors de la première connexion, le bot créera une base de données sqlite3 où il stockera les requêtes des utilisateurs.

Le préfixe des commandes du bot est: `*`

Vous pouvez utiliser la commande `*help` pour que le bot vous envoie la liste des commandes disponibles en message privé.